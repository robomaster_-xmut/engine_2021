#include "remote.h"
#include "bsp_can.h"
#include "pid.h"
#include "tim.h"
#include "math.h"

uint8_t key_W,key_S,key_A,key_D,key_SHIFT,key_CTRL,key_Q,key_E,key_R,key_F,key_G,key_Z,key_X,key_C,key_V;
float remote_speed1=0,remote_speed2=0,remote_speed3=0;    //电机四个车轮速度值（以最大值百分比表示）
//static char adjust_remote_f=0;      //遥控器是否校准标识
uint8_t flag_1=0,flag_2=1;
extern float ch[7];
extern moto_info_t motor_info[MOTOR_MAX_NUM];
extern moto_info_t motor_info1[MOTOR_MAX_NUM];
extern pid_struct_t motor_pid[7];
extern pid_struct_t Ctrl_pid[2];
extern uint16_t ring;
extern uint16_t ring1;
int32_t ting=2000; //底盘初始速度
int32_t s = 0;
int32_t shang = 0;
int32_t err = 0;
int32_t err1 = 0;
int32_t come_back = 0;
int32_t UP_DOWN = 0;
uint8_t	no_target=0;
uint8_t	have_goal=0;
uint8_t Goal_State=1;
uint8_t SPEED_=1;
uint8_t biao_S=1;
uint8_t biao_C=1;
uint8_t biao_L=0;
uint8_t biao_L1=0;
uint8_t biao_x1=0;
uint8_t biao_R=1;
uint8_t biao_R1=1;
uint8_t visual=1;
int16_t W_speed=0;
int16_t A_speed=0;
int16_t S_speed=0;
int16_t D_speed=0;
int16_t Q_speed=0;
int16_t E_speed=0;
int16_t Help_speed_R=0;
int16_t Help_speed_F=0;
int MODE=0;
uint16_t Help_speed=400;
uint16_t j=0;
int _3508_Speedmax=8000;
int16_t PICH_err=0;
int16_t YAW_err=0;
int16_t PICH_err1=0;
int16_t YAW_err1=0;
int16_t PICH=180;
int16_t YAW=180;
int16_t platform_Q=175;
int16_t HELP=180;
int16_t platform_H=175;
int Card=0;
int Maganize=0;
int Goal_State11=0;
RC_Type remote_control;
uint32_t  Latest_Remote_Control_Pack_Time = 0;
uint32_t  LED_Flash_Timer_remote_control = 0;
uint32_t speed_time=0,mouse_time=0;

void Callback_RC_Handle(RC_Type* rc, uint8_t* buff)
{
//	rc->ch1 = (*buff | *(buff+1) << 8) & 0x07FF;	offset  = 1024
	rc->ch1 = (buff[0] | buff[1]<<8) & 0x07FF;
	rc->ch1 -= 1024;
	rc->ch2 = (buff[1]>>3 | buff[2]<<5 ) & 0x07FF;
	rc->ch2 -= 1024;
	rc->ch3 = (buff[2]>>6 | buff[3]<<2 | buff[4]<<10) & 0x07FF;
	rc->ch3 -= 1024;
	rc->ch4 = (buff[4]>>1 | buff[5]<<7) & 0x07FF;		
	rc->ch4 -= 1024;
	
	rc->switch_left = ( (buff[5] >> 4)& 0x000C ) >> 2;
	rc->switch_right =  (buff[5] >> 4)& 0x0003 ;
	
	rc->sw = (buff[16] | (buff[17] << 8)) & 0x07FF;
	rc->sw -= 1024;
	
	rc->mouse.x = buff[6] | (buff[7] << 8);	//x axis
	rc->mouse.y = buff[8] | (buff[9] << 8);
	rc->mouse.z = buff[10]| (buff[11] << 8);
	
	rc->mouse.press_left 	= buff[12];	// is pressed?
	rc->mouse.press_right = buff[13];
	
	rc->keyBoard.key_code = buff[14] | buff[15] << 8; //key borad code
	
	
}
int i;
void RC_control (void)
{ 
	Latest_Remote_Control_Pack_Time = HAL_GetTick();
	
	if(remote_control.switch_right==0x01&&remote_control.switch_left==0x01)MODE=1;
  else if(remote_control.switch_right==0x01&&remote_control.switch_left==0x03)MODE=2;
  else if(remote_control.switch_right==0x03&&remote_control.switch_left==0x01)MODE=3;
	else MODE=0;
   	/****************数据处理***************/
	
	/*******模式0解析*****/// l:2 r:1 
	if(MODE==0){        
		/*右前轮*/ch[0]= -remote_control.ch4*8000/660+remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*右后轮*/ch[1]= -remote_control.ch4*8000/660-remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*左后轮*/ch[2]= remote_control.ch4*8000/660-remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*左前轮*/ch[3]= remote_control.ch4*8000/660+remote_control.ch3*8000/660+remote_control.ch1*8000/660;
			ch[4]=remote_control.ch2*2000/660;
			ch_CAN2[0]=0;ch_CAN2[1]=0;ch_CAN2[2]=0;ch_CAN2[3]=0;
			if(ch[0]>_3508_Speedmax) ch[0]=_3508_Speedmax;  if(ch[0]<-_3508_Speedmax) ch[0]=-_3508_Speedmax;
			if(ch[1]>_3508_Speedmax) ch[1]=_3508_Speedmax;  if(ch[1]<-_3508_Speedmax) ch[1]=-_3508_Speedmax;
			if(ch[2]>_3508_Speedmax) ch[2]=_3508_Speedmax;  if(ch[2]<-_3508_Speedmax) ch[2]=-_3508_Speedmax;
			if(ch[3]>_3508_Speedmax) ch[3]=_3508_Speedmax;  if(ch[3]<-_3508_Speedmax) ch[3]=-_3508_Speedmax;
	}
	
	/*******模式1解析*****/// l:1 r:1
	if(MODE==1){        //  1 * 1             use：ch3、ch4
		 ch[0]=0;ch[1]=0;ch[2]=0;ch[3]=0;ch[4]=0;
			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
			ch_CAN2[1]= remote_control.ch4*2000/660;
			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
			ch_CAN2[3]= remote_control.ch3*2000/660;
    
	 	 PICH=-remote_control.ch2*120/660+180;
		 __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_1, PICH+50);  //修改pwm占空比
	 	 YAW=-remote_control.ch1*120/660+180;
     __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_2, YAW);  //修改pwm占空比
    
    

//		if(ring>31){
//			ch_CAN2[0]=0;
//			ch_CAN2[1]=0;
//		}
//		else{
//			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
//			ch_CAN2[1]= remote_control.ch4*2000/660;
//		}
//		if(ring1>9){
//			ch_CAN2[2]=0;
//			ch_CAN2[3]=0;
//		}
//		else{
//			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
//			ch_CAN2[3]= remote_control.ch3*2000/660;
//		}
		if(remote_control.sw<-100){
			
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_RESET);//闭
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_SET);
		}
		if(remote_control.sw<-600){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);
		}
		if(remote_control.sw>100){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_SET);//开
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_RESET);
		}
		if(remote_control.sw>600){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
		}
		 ring=0;
		 ring1=0;
		 biao_L=0;
		 biao_R=1;
	}
	
	
	/*******模式2解析*****/// l:3 r:1
	if(MODE==2){              //  3 * 1
		ch[0]=0;ch[1]=0;ch[2]=0;ch[3]=0;ch[4]=0;
		ch_CAN2[0]=0;ch_CAN2[1]=0;ch_CAN2[2]=0;ch_CAN2[3]=0;
	 	 YAW=-remote_control.ch2*120/660+175;
	 	 PICH=+remote_control.ch2*120/660+175;
     __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, PICH);  //修改pwm占空比
     __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, YAW);  //修改pwm占空比
     if(remote_control.ch1>600){Maganize=200;Card=100;}
     if(remote_control.ch1<-600){Maganize=140;Card=160;}
     __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_4, Card);  //修改pwm占空比
     __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, Maganize);  //修改pwm占空比
		 if(remote_control.sw>500)
			 HELP=50;
		 else
			 HELP=180;
		 __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, HELP);  //修改pwm占空比
		 ring=0;
		 ring1=0;
		 biao_L=0;
		 biao_R=1;
	}
	
	
	/*******模式3解析*****/// l:1 r:3
	if(MODE==3){                // 1 * 3
		/*******键盘解析*****/
		if(Latest_Remote_Control_Pack_Time - speed_time >100){
			speed_time = Latest_Remote_Control_Pack_Time;
	//		key_W 
			if((remote_control.keyBoard.key_code>>0)&0x0001){
				W_speed+=10;if(W_speed>100)W_speed=100;
			}else {
				W_speed-=30;if(W_speed<0)W_speed=0;
			}
	//		key_S 
			if((remote_control.keyBoard.key_code>>1)&0x0001){
				S_speed+=10;if(S_speed>100)S_speed=100;
			}else {
				S_speed-=30;if(S_speed<0)S_speed=0;
			}
	//		key_A 
			if((remote_control.keyBoard.key_code>>2)&0x0001){
				A_speed+=10;if(A_speed>100)A_speed=100;
			}else {
				A_speed-=30;if(A_speed<0)A_speed=0;
			}
	//		key_D 
			if((remote_control.keyBoard.key_code>>3)&0x0001){
				D_speed+=10;if(D_speed>100)D_speed=100;
			}else {
				D_speed-=30;if(D_speed<0)D_speed=0;
			}
	//		key_Q 
			if((remote_control.keyBoard.key_code>>6)&0x0001){
				Q_speed+=10;if(Q_speed>100)Q_speed=100;
			}else {
				Q_speed-=30;if(Q_speed<0)Q_speed=0;
			}
	//		key_E 
			if((remote_control.keyBoard.key_code>>7)&0x0001){
				E_speed+=10;if(E_speed>100)E_speed=100;
			}else {
				E_speed-=30;if (E_speed<0)E_speed=0;
			}
		}
		key_SHIFT = (remote_control.keyBoard.key_code>>4)&0x0001;
		if((key_SHIFT)&&biao_S){SPEED_++;SPEED_%=6;biao_S=0;}
		else if(!key_SHIFT)biao_S=1;
		key_CTRL = (remote_control.keyBoard.key_code>>5)&0x0001;
		if((key_CTRL)&&biao_C){SPEED_--;if(SPEED_<1)SPEED_=1;biao_C=0;}
		else if(!key_CTRL)biao_C=1;
		
//		key_R 
		if((remote_control.keyBoard.key_code>>8)&0x0001)Help_speed_R=400;else Help_speed_R=0;
//		key_F 
 		if((remote_control.keyBoard.key_code>>9)&0x0001)Help_speed_F=-400;else Help_speed_F=0;
//		key_G 
    if(((remote_control.keyBoard.key_code>>10)&0x0001)&&Goal_State){
			have_goal++;have_goal%=2;Goal_State=0;
			switch(have_goal){
				case 0:Maganize=200;Card=100;break;
				case 1:Maganize=140;Card=160;break;
			}
		}else if(!((remote_control.keyBoard.key_code>>10)&0x0001))
			Goal_State=1;
//		key_Z 
    if(((remote_control.keyBoard.key_code>>11)&0x0001)&&biao_R){
			biao_L++;biao_L%=3;biao_R=0;biao_R1=0;if(biao_L==0)biao_R=1;ring=0;ring1=0;
		}
		if(biao_L==1){
			if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_5)){ch_CAN2[0]= -1000;ch_CAN2[1]= 1000;
			}else {ch_CAN2[0]=0;ch_CAN2[1]=0;
			}
			if(ring1>=9){
				ch_CAN2[2]=0;ch_CAN2[3]=0;
			}else{
				ch_CAN2[2]= 1000;ch_CAN2[3]= -1000;
				if(motor_info1[2].torque_current>13000){
				ch_CAN2[2]=0;ch_CAN2[3]=0;biao_R1=1;}
			}
			if(ring1>=9){
				biao_R=1;
			}
		}else if(biao_L==2){
			if(ring>=30){
				ch_CAN2[0]=0;ch_CAN2[1]=0;
			}else{
				ch_CAN2[0]= 1000;ch_CAN2[1]= -1000;
			}
			if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_4)){ch_CAN2[2]= -600;ch_CAN2[3]= 600;
			}else {ch_CAN2[2]=0;ch_CAN2[3]=0;
			}
			if(ring>=30){
				biao_R=1;
			}
		}
		
    if(((remote_control.keyBoard.key_code>>12)&0x0001)&&biao_x1){
			flag_1++;if(flag_1>2)flag_1=1;
		}if(remote_control.mouse.press_right)biao_x1=0;else biao_x1=1;
		if(flag_1==1){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_SET);//开
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_RESET);
//			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
//			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
		}else if(flag_1==2){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_RESET);//闭
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_SET);
//			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
//			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);
		}
			if(remote_control.mouse.press_left){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
			}else 
			{
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);}
//		key_C =    (remote_control.keyBoard.key_code>>13)&0x0001;
//		key_V     
		if((remote_control.keyBoard.key_code>>14)&0x0001)HELP=50;else HELP=180;
		
		
		/*******底盘控制*****/
		/*右前轮*/ch[0]= (-W_speed+S_speed-A_speed+D_speed-Q_speed+E_speed)*SPEED_*10;
		/*右后轮*/ch[1]= (-W_speed+S_speed+A_speed-D_speed-Q_speed+E_speed)*SPEED_*10;
		/*左后轮*/ch[2]= (W_speed-S_speed+A_speed-D_speed-Q_speed+E_speed)*SPEED_*10;
		/*左前轮*/ch[3]= (W_speed-S_speed-A_speed+D_speed-Q_speed+E_speed)*SPEED_*10;
		ch[4]=Help_speed_R+Help_speed_F;
		for(uint8_t i=0;i<4;i++)
		{  if(ch[i]>6000)ch[i]=6000;
			if(ch[i]<-6000)ch[i]=-6000;
		}
		
		/*******云台控制*****//*******鼠标解析*****/
		if(Latest_Remote_Control_Pack_Time - mouse_time >100){
			mouse_time = Latest_Remote_Control_Pack_Time;
			if(remote_control.mouse.x>10){
				YAW_err-=10;if(YAW_err<-120)YAW_err=-120;
			}
			else if(remote_control.mouse.x<-10){
				YAW_err+=10;if(YAW_err>120)YAW_err=120;
			}
			if(remote_control.mouse.y>10){
				PICH_err-=10;if(PICH_err<-120)PICH_err=-120;
			}
			else if(remote_control.mouse.y<-10){
				PICH_err+=10;if(PICH_err>120)PICH_err=120;
			}
		}
		if((remote_control.mouse.press_right)&&biao_L1){visual++;if(visual>3)visual=1;}//Ctrl键减挡位
			if(remote_control.mouse.press_right)biao_L1=0;else biao_L1=1;
	 if(!biao_L1){
	switch(visual){
		case 1:YAW_err=0;PICH_err=0;break;
		case 2:YAW_err=0;PICH_err=50;break;
		case 3:YAW_err=60;PICH_err=0;break;
	}}
//		if(remote_control.mouse.press_right){YAW_err=0;PICH_err=0;}
		YAW = 180+YAW_err;PICH = 180+PICH_err;
		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_1, PICH);  //修改pwm占空比
		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_2, YAW);  //修改pwm占空比
		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_4, Card);  //修改pwm占空比
    __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, Maganize);  //修改pwm占空比
		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, platform_H);  //修改pwm占空比
		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, platform_Q);  //修改pwm占空比
		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, HELP);  //修改pwm占空比
		
//		if(ring>10){
//			ch_CAN2[0]=0;
//			ch_CAN2[1]=0;
//		}
//		else{
//			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
//			ch_CAN2[1]= remote_control.ch4*2000/660;
//		}
//		if(ring1>7){
//			ch_CAN2[2]=0;
//			ch_CAN2[3]=0;
//		}
//		else{
//			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
//			ch_CAN2[3]= remote_control.ch3*2000/660;
//		}
	}
 
	
}

