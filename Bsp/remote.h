#ifndef _REMOTE_H
#define _REMOTE_H

#include "main.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"
typedef struct {
	int16_t ch1;	//each ch value from -364 -- +364
	int16_t ch2;
	int16_t ch3;
	int16_t ch4;
	
	uint8_t switch_left;	//3 value
	uint8_t switch_right;
	
	int16_t sw;
	struct {
		int16_t x;
		int16_t y;
		int16_t z;
	
		uint8_t press_left;
		uint8_t press_right;
	}mouse;
	
	struct {
		uint16_t key_code;
/**********************************************************************************
   * 键盘通道:15   14   13   12   11   10   9   8   7   6     5     4   3   2   1
   *          V    C    X	  Z    G    F    R   E   Q  CTRL  SHIFT  D   A   S   W
************************************************************************************/

	}keyBoard;
	

}RC_Type;
void RemoteDataPrcess(uint8_t *pData);//数据处理(处理+校验)
void RC_control (void);
void Callback_RC_Handle(RC_Type* rc, uint8_t* buff);
void Turning_Laps(uint16_t num,float speed);
#endif  
