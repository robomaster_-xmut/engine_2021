/****************************************************************************
 *  Copyright (C) 2018 RoboMaster.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
 
#include "pid.h"
#include "math.h"
/**
  * @brief  init pid parameter
  * @param  pid struct
    @param  parameter
  * @retval None
  */
void pid_init(PID_ID mode,
	            pid_struct_t *pid,
              float kp,
              float ki,
              float kd,
              float i_max,
              float out_max,
              float DeadBand)
{
  pid->kp      = kp;
  pid->ki      = ki;
  pid->kd      = kd;
  pid->i_max   = i_max;
  pid->out_max = out_max;
  pid->DeadBand= DeadBand;
	pid->mode    =mode;
}

///**
//  * @brief  pid calculation
//  * @param  pid struct
//    @param  reference value
//    @param  feedback value
//  * @retval calculation result
//  */
//float pid_calc(pid_struct_t *pid, float ref, float fdb)
//{
//  pid->ref = ref;
//  pid->fdb = fdb;
//  pid->err[1] = pid->err[0];
//  pid->err[0] = pid->ref - pid->fdb;
//  
//  pid->p_out  = pid->kp * pid->err[0];
//  pid->i_out += pid->ki * pid->err[0];
//  pid->d_out  = pid->kd * pid->err[0] - pid->err[1];
//  LIMIT_MIN_MAX(pid->i_out, -pid->i_max, pid->i_max);
//  
//  pid->output = pid->p_out + pid->i_out + pid->d_out;
//  LIMIT_MIN_MAX(pid->output, -pid->out_max, pid->out_max);
//  return pid->output;
//}



/**
  * @brief  pid calculation
  * @param  pid struct
    @param  reference value
    @param  feedback value
  * @retval calculation result
  */
float pid_calc(pid_struct_t *pid, float ref, float fdb)
{
  pid->ref = ref;
  pid->fdb = fdb;
  pid->err[1] = pid->err[0];
  pid->err[0] = pid->ref - pid->fdb;
	
	if(fabsf(pid->err[0])> pid->DeadBand||pid->DeadBand==0)
	{
	if(pid->mode==PID_Position)
  {if(fabsf(pid->err[0])>4096)
	{
	  if(pid->err[0]>0)pid->err[0]=-(8191-pid->err[0]);
	  else pid->err[0]=(8191+pid->err[0]);
	}
	}
  pid->p_out  = pid->kp * pid->err[0];
  pid->i_out += pid->ki * pid->err[0];
  pid->d_out  = pid->kd * pid->err[0] - pid->err[1];
  LIMIT_MIN_MAX(pid->i_out, -pid->i_max, pid->i_max);
  
  pid->output = pid->p_out + pid->i_out + pid->d_out;	
	
  LIMIT_MIN_MAX(pid->output, -pid->out_max, pid->out_max);
  }
	else{
        pid->output=0;
	}

  return pid->output;
}

/**
  * @brief  pid calculation
  * @param  pid struct
    @param  reference value
    @param  feedback value
  * @retval calculation result
  */
float pid_calc1(pid_struct_t *pid, float ref, float fdb)
{
  pid->ref = ref;
  pid->fdb = fdb;
  pid->err[1] = pid->err[0];
  pid->err[0] = pid->ref - pid->fdb;
	
	if(fabsf(pid->err[0])> pid->DeadBand||pid->DeadBand==0)
	{
	if(pid->mode==PID_Position)
  {if(fabsf(pid->err[0])>4096)
	{
	  if(pid->err[0]>0)pid->err[0]=-(8191-pid->err[0]);
	  else pid->err[0]=(8191+pid->err[0]);
	}
	}
  pid->p_out  = pid->kp * pid->err[0];
  pid->i_out += pid->ki * pid->err[0];
  pid->d_out  = pid->kd * (pid->err[0] - pid->err[1]);
  LIMIT_MIN_MAX(pid->i_out, -pid->i_max, pid->i_max);
  
  pid->output = pid->p_out + pid->i_out + pid->d_out;	
	
  LIMIT_MIN_MAX(pid->output, -pid->out_max, pid->out_max);
  }
	else{
        pid->output=0;
	}

  return pid->output;
}   


