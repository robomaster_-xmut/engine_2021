/****************************************************************************
 *  Copyright (C) 2018 RoboMaster.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
 
#ifndef _PID_H
#define _PID_H

#include "main.h"
typedef enum
{

	PID_Position,
	PID_Speed
	
}PID_ID;
typedef struct _pid_struct_t
{
	PID_ID mode;
  float kp;
  float ki;
  float kd;
  float i_max;
  float out_max;
  float DeadBand;
	
  float ref;      // target value
  float fdb;      // feedback value
  float err[2];   // error and last error

  float p_out;
  float i_out;
  float d_out;
  float output;
}pid_struct_t;

void pid_init(PID_ID mode,
	            pid_struct_t *pid,
              float kp,
              float ki,
              float kd,
              float i_max,
              float out_max,
              float DeadBand
						  );
              
float  pid_calc(pid_struct_t *pid, float ref, float fdb);
float pid_calc1(pid_struct_t *pid, float ref, float fdb);
//float pid_calc_weizi(pid_struct_t *pid, float ref, float fdb);
#endif
